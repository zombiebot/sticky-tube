#include <QApplication>
#include "html5applicationviewer.h"
#include <QWebSettings>
#include <QMainWindow>
#include <QMenuBar>
#include <QMenu>
#include <QAction>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QMainWindow w;
    QWebSettings::globalSettings()->setAttribute(QWebSettings::PluginsEnabled,true);
    QCoreApplication::setApplicationName("Sticky Tube");

    Html5ApplicationViewer viewer;
    w.setCentralWidget(&viewer);
    w.resize(560, 345);
    w.setWindowFlags(Qt::WindowStaysOnTopHint);
    QMenu *fileMenu = w.menuBar()->addMenu("&File");
    QAction *exitAction = fileMenu->addAction("&Exit");
    QObject::connect(exitAction, SIGNAL(triggered()), qApp, SLOT(quit()));

    viewer.setOrientation(Html5ApplicationViewer::ScreenOrientationAuto);
    viewer.loadFile(QLatin1String("html/index.html"));

    w.show();

    return app.exec();
}



